package Database;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.placeits.PlaceIt;
import com.example.placeits.userendpoint.Userendpoint;
import com.example.placeits.categoryplaceitendpoint.Categoryplaceitendpoint;
import com.example.placeits.placeitendpoint.Placeitendpoint;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.json.gson.GsonFactory;
public class MainDataSource extends Observable
{
	private static final String LOGTAG = "Main Db creation";
	private static MainDataSource instance = null;
	SQLiteOpenHelper dataBaseHelper;
	SQLiteDatabase dataBase;
	
	public static final String[] allColumns = 
	{
		MainDatabase.COLUMN_DroppinID,
		MainDatabase.COLUMN_Latitude,
		MainDatabase.COLUMN_Longitude,
		MainDatabase.COLUMN_Pintitle,
		MainDatabase.COLUMN_onSchedule,
		MainDatabase.COLUMN_Pindescription,
		MainDatabase.COLUMN_Pinstatus,
		MainDatabase.COLUMN_schedule,
		MainDatabase.COLUMN_inactiveDate
	};
	
	private MainDataSource(Context context)
	{
		dataBaseHelper = new MainDatabase(context);	
	}
	public static MainDataSource getInstance(Context context)
	{
		if (instance == null)
			instance = new MainDataSource(context);
		return instance;
	}
	
	public void open()
	{
		Log.i(LOGTAG,"Database opened");
		dataBase = dataBaseHelper.getReadableDatabase();
	}
	
	public void close()
	{
		Log.i(LOGTAG,"Database closed");
		dataBaseHelper.close();
	}
	
	public int create(PlaceIt place_it)
	{
		
		Placeitendpoint.Builder builder = new Placeitendpoint.Builder(
				  AndroidHttp.newCompatibleTransport(), new GsonFactory(), null);
				Placeitendpoint service = builder.build();
				
		ContentValues values = new ContentValues();
		values.put(MainDatabase.COLUMN_Latitude	, place_it.getLatitude());
		values.put(MainDatabase.COLUMN_Longitude, place_it.getLongitude());
		values.put(MainDatabase.COLUMN_Pintitle	, place_it.getTitle());
		values.put(MainDatabase.COLUMN_Pindescription, place_it.getDescription());
		values.put(MainDatabase.COLUMN_Pinstatus, place_it.getStatus());
		values.put(MainDatabase.COLUMN_onSchedule	, place_it.getOnSchedule());
		values.put(MainDatabase.COLUMN_schedule	, place_it.getSchedule());
		dataBase.insert(MainDatabase.TABLE_Droppins, null, values);
		
		Cursor row_id = dataBase.rawQuery("select "+ MainDatabase.COLUMN_DroppinID +" from "+MainDatabase.TABLE_Droppins +" order by "+MainDatabase.COLUMN_DroppinID +" DESC limit 1", null);
		
		setChanged();
		row_id.moveToNext();
		notifyObservers(row_id.getInt(0));
		return row_id.getInt(0);
	}
	
	public List<PlaceIt> findAll()
	{
		Cursor cursor = dataBase.query(MainDatabase.TABLE_Droppins, allColumns, null, null, null, null, null);
		Log.i(LOGTAG,"Returned"+cursor.getCount()+" rows");
		List<PlaceIt> place_its = cursorToList(cursor);
	
		return place_its;
	}
	
	public List<PlaceIt> findOnSchedule()
	{
		Cursor cursor = dataBase.query(MainDatabase.TABLE_Droppins,allColumns, "'" +MainDatabase.COLUMN_onSchedule +"' = 1 AND '"+MainDatabase.COLUMN_Pinstatus +"' = INACTIVE" , null, null, null, null, null);
		Log.i(LOGTAG,"Returned"+cursor.getCount()+" rows");
		List<PlaceIt> place_its = cursorToList(cursor);
	
		return place_its;
	}
	
	public PlaceIt findByLatLong(double latitude, double longitude)
	{ 
		Cursor cursor = dataBase.rawQuery("select * from "+MainDatabase.TABLE_Droppins  + " where "+MainDatabase.COLUMN_Latitude +" = "+ latitude +" AND "+ MainDatabase.COLUMN_Longitude +" = "+ longitude, null);
		Log.i(LOGTAG,"Returned"+cursor.getCount()+" rows");
		List<PlaceIt> place_its = cursorToList(cursor);
		return place_its.get(0);
	}
	
	public List<PlaceIt> findByPinId(int pinId)
	{
		String query = MainDatabase.COLUMN_DroppinID+" = "+"'"+pinId+"'";
		Cursor cursor = dataBase.query(MainDatabase.TABLE_Droppins,allColumns,query, null, null, null, null);
		Log.i(LOGTAG,"Returned"+cursor.getCount()+" rows");
		List<PlaceIt> place_its = cursorToList(cursor);
	
		return place_its;
	}
	
	public List<PlaceIt> findByStatus(String status)
	{
		String query = MainDatabase.COLUMN_Pinstatus +"= '" + status +"'" ;
		Cursor cursor = dataBase.query(MainDatabase.TABLE_Droppins, allColumns, query, null, null, null, null);
		Log.i(LOGTAG,"Returned"+cursor.getCount()+" rows");
		List<PlaceIt> place_its = cursorToList(cursor);
	
		return place_its;
	}
	
	public void editById(PlaceIt place_it)
	{
		dataBase.rawQuery(" ", null);
		ContentValues values = new ContentValues();
		values.put(MainDatabase.COLUMN_Latitude	, place_it.getLatitude());
		values.put(MainDatabase.COLUMN_Longitude, place_it.getLongitude());
		values.put(MainDatabase.COLUMN_Pintitle	, place_it.getTitle());
		values.put(MainDatabase.COLUMN_Pindescription, place_it.getDescription());
		values.put(MainDatabase.COLUMN_Pinstatus, place_it.getStatus());
		values.put(MainDatabase.COLUMN_onSchedule	, place_it.getOnSchedule());
		values.put(MainDatabase.COLUMN_schedule	, place_it.getSchedule());
		values.put(MainDatabase.COLUMN_inactiveDate	, place_it.getInactiveDateTime().toString());
		dataBase.update(MainDatabase.TABLE_Droppins, values, MainDatabase.COLUMN_DroppinID +" = " + "'"+place_it.getPlaceItId() +"'", null);
		setChanged();
		notifyObservers(place_it.getPlaceItId());
	}
	
	public void deleteById(int place_it_id)
	{
		dataBase.delete(MainDatabase.TABLE_Droppins,MainDatabase.COLUMN_DroppinID +"=" + place_it_id,null);
		setChanged();
		notifyObservers(place_it_id);
	}
	
	private List<PlaceIt> cursorToList(Cursor cursor)
	{
		List<PlaceIt> place_its = new ArrayList<PlaceIt>();
		if(cursor.getCount()>0)
		{
			while (cursor.moveToNext())
			{
				PlaceIt place_it = new PlaceIt();
				place_it.setTitle(cursor.getString(cursor.getColumnIndex(MainDatabase.COLUMN_Pintitle)));
				place_it.setDescription(cursor.getString(cursor.getColumnIndex(MainDatabase.COLUMN_Pindescription)));
				place_it.setLongitude(cursor.getDouble(cursor.getColumnIndex(MainDatabase.COLUMN_Longitude)));
				place_it.setLatitude(cursor.getDouble(cursor.getColumnIndex(MainDatabase.COLUMN_Latitude)));
				place_it.setStatus(cursor.getString(cursor.getColumnIndex(MainDatabase.COLUMN_Pinstatus)));
				place_it.setOnSchedule((int)cursor.getLong(cursor.getColumnIndex(MainDatabase.COLUMN_onSchedule)));
				place_it.setSchedule(cursor.getInt(cursor.getColumnIndex(MainDatabase.COLUMN_schedule)));
				place_it.setPlaceItId((int)cursor.getLong(cursor.getColumnIndex(MainDatabase.COLUMN_DroppinID)));
				place_its.add(place_it);
				
			}
		}
		return place_its;
	}
}
