package com.example.placeits;


import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import Database.MainDataSource;

import com.example.placeits.PlaceIt;

public class ActiveListActivity extends ListActivity
{
	private List<PlaceIt> placeItsList;
	MainDataSource dataSource = MainDataSource.getInstance(this);
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{    	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_lists);
		TextView listTitle = (TextView)findViewById(R.id.listTitle);
		listTitle.setText("The following Place It(s) are active:");
		
		placeItsList = dataSource.findByStatus("ACTIVE");

		if(placeItsList.size() > 0)
		{
			PlaceItAdapter adaptedPlaceIts = new PlaceItAdapter (this,android.R.layout.simple_list_item_1, placeItsList);
			setListAdapter(adaptedPlaceIts);
		}
	 }
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id)
	{
		super.onListItemClick(l, v, position, id);
		PlaceIt placeIt = placeItsList.get(position);
		Intent intent = new Intent(this, ViewPlaceItActivity.class);
		intent.putExtra("placeIt", (Parcelable) placeIt);
		startActivity(intent);
		finish();
	}
}