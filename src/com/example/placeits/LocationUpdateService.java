package com.example.placeits;


import java.util.ArrayList;
import java.util.List;

import Database.MainDataSource;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;


public class LocationUpdateService extends Service implements LocationListener
{	
	private Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	protected MainDataSource dataSource = MainDataSource.getInstance(this);
	private int notificationId = 0;
	private final int TIME_SENSITIVITY = 30000; // milliseconds
	private final int DISTANCE_SENSITIVITY = -1;  // meters. if negative, then distance is not used, only the time.
	private final int ALERT_DISTANCE = 1000; // meters
	
	@Override
	public IBinder onBind(Intent arg0)
	{
		return null;
	}
	
	@Override
	public void onLocationChanged(Location currentLoc)
	{
		float[] results = new float[3];
		ArrayList<PlaceIt> triggeredPlaceIts = new ArrayList<PlaceIt>();
		List<PlaceIt> marker_list  = dataSource.findByStatus("ACTIVE");
		if(marker_list.size() > 0 )
     	{
			for (PlaceIt PlaceIt : marker_list)
			{
				Location.distanceBetween(currentLoc.getLatitude(), currentLoc.getLongitude(), PlaceIt.getLatitude(), PlaceIt.getLongitude(), results);
				if (results[0] < ALERT_DISTANCE) // results[0] holds the computed distance between the Place It and yourself
					triggeredPlaceIts.add(PlaceIt);
			}
			if (triggeredPlaceIts.size() > 0) {
				NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
					.setSmallIcon(R.drawable.note_icon)
					.setContentTitle("ALERT!")
					.setContentText("PlaceIt(s) Nearby")
					.setAutoCancel(true)
					.setSound(alarmSound);
				Intent baseIntent = new Intent(this, MainActivity.class);
				Intent topIntent = new Intent(this, TriggeredPlaceItsActivity.class);
				Bundle bun = new Bundle();
				bun.putParcelableArrayList("triggeredPlaceIts", triggeredPlaceIts);
				topIntent.putExtras(bun);
				TaskStackBuilder stack_builder = TaskStackBuilder.create(this);
				stack_builder.addNextIntent(baseIntent);
				stack_builder.addNextIntent(topIntent);
				PendingIntent pending_intent = stack_builder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
				builder.setContentIntent(pending_intent);
				NotificationManager notification_manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
				notification_manager.notify(notificationId,builder.build());	
			}
		}
	}
	
	@Override
    public void onCreate()
	{
		LocationManager location_manager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
		location_manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, TIME_SENSITIVITY, DISTANCE_SENSITIVITY, (android.location.LocationListener) this);
		location_manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, TIME_SENSITIVITY, DISTANCE_SENSITIVITY, (android.location.LocationListener) this);
    }
	
	@Override
	public void onProviderDisabled(String provider) {}
	
	@Override
	public void onProviderEnabled(String provider) {}
	
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}

}