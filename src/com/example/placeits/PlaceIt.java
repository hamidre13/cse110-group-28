package com.example.placeits;

import java.util.Calendar;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

public class PlaceIt implements Parcelable{
	private String title;
	private String description;
	private double longitude;
	private double latitude;
	private int onSchedule;
	private int schedule=0;
	private int placeItId = -1;
	private Calendar inactiveDateTime = Calendar.getInstance();
	private String status;
	private boolean isCategory = false;
	private String category1 = "";
	private String category2 = "";
	private String category3 = "";
	
	public PlaceIt(Parcel in)
	{
		readFromParcel(in);
	}
	
	public PlaceIt(LatLng point)
	{
		latitude = point.latitude;
		longitude = point.longitude;
	}
	
	public PlaceIt()
	{
		super();
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public int getOnSchedule() {
		return onSchedule;
	}
	public void setOnSchedule(int onSchedule) {
		this.onSchedule = onSchedule;
	}
	public int getSchedule() {
		return schedule;
	}
	public void setSchedule(int schedule) {
		this.schedule = schedule;
	}
	public int getPlaceItId() {
		return placeItId;
	}
	public void setPlaceItId(int placeItId) {
		this.placeItId = placeItId;
	}
	public Calendar getInactiveDateTime() {
		return inactiveDateTime;
	}
	public void setInactiveDateTime(Calendar inactiveDateTime) {
		this.inactiveDateTime = inactiveDateTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isCategory() {
		return isCategory;
	}

	public void setCategory(boolean isCategory) {
		this.isCategory = isCategory;
	}

	public String getCategory1() {
		return category1;
	}
	public void setCategory1(String category1) {
		this.category1 = category1;
	}
	public String getCategory2() {
		return category2;
	}
	public void setCategory2(String category2) {
		this.category2 = category2;
	}
	public String getCategory3() {
		return category3;
	}
	public void setCategory3(String category3) {
		this.category3 = category3;
	}
	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public void writeToParcel(Parcel out, int flags)
	{
		out.writeDouble(longitude);
		out.writeDouble(latitude);	
		out.writeString(title);
		out.writeByte((byte)(isCategory ? 1: 0));
		out.writeInt(placeItId);
	}
	
	private void readFromParcel(Parcel in)
	{
		longitude = in.readDouble();
		latitude = in.readDouble();
		title = in.readString();
		isCategory = in.readByte() != 0;
		placeItId = in.readInt();
	}
	
	public static final Parcelable.Creator<PlaceIt> CREATOR = new Parcelable.Creator<PlaceIt>()
	{
		public PlaceIt createFromParcel (Parcel in)
		{
			return new PlaceIt(in);
		}

		public PlaceIt[] newArray(int size)
		{
			return new PlaceIt[size];
		}
	};	
}
