package com.example.placeits;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class SignUpActivity extends Activity
{
	private EditText userNameEdit;
	private EditText passwordEdit;
	private EditText passwordConfirmEdit;
	private Button signUpButton;
	private Button cancelButton;
	
	protected void onCreate (Bundle savedInstanceState)
	{
		 super.onCreate(savedInstanceState);
	     setContentView(R.layout.sign_in);
	     
	     setUpUIElements();
	}
	
	private View.OnClickListener onClickListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			switch(v.getId())
			{
				case R.id.sign_up_button:
					/** TODO
					 * 		1. Extract the typed-in user name from userNameEdit, and check to see if the user name exists.
					 * 			a. User name exists: Show a TOAST message displaying: "User name is already taken."
					 * 			b. User name does not exist: Move to #2.
					 * 		2. Extract the typed-in passwords from passwordEdit and passwordConfirmEdit, and check to see if the passwords match.
					 * 			a. Passwords do not match: Show a TOAST message displaying: "Different passwords were entered."
					 * 			b. Passwords match: Move to #3.
					 * 		3. Store the user name and password into whatever is holding that information, and return to the SignInActivity.
					 * 			Also, display a TOAST message which notifies the user that his/her account was created.
					 */
					String user_name = userNameEdit.getText().toString();
					String password = passwordEdit.getText().toString();
					String password_confirm = passwordConfirmEdit.getText().toString();
					
					
				break;
				
				case R.id.cancel_button:
					finish();
				break;
			}
		}
	};

	private void setUpUIElements()
	{
		userNameEdit = (EditText) findViewById(R.id.user_name_edit);
		passwordEdit = (EditText) findViewById(R.id.password_edit);
		passwordConfirmEdit = (EditText) findViewById(R.id.password_confirm_edit);
		signUpButton = (Button) findViewById(R.id.sign_up_button);
		cancelButton = (Button) findViewById(R.id.cancel_button);
		
		signUpButton.setOnClickListener(onClickListener);
		cancelButton.setOnClickListener(onClickListener);
	}
}
