package com.example.placeits;

import java.util.Calendar;
import java.util.GregorianCalendar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import Database.MainDataSource;

	
public class ViewPlaceItActivity extends Activity
{	
	private Button setActiveButton;
	private Button setInactive;
	private Button deleteButton;
	private PlaceIt placeIt;
	private MainDataSource dataSource = MainDataSource.getInstance(this);
	
	protected void onCreate(Bundle savedInstanceState)
	{	
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        PlaceIt place_it = i.getParcelableExtra("placeIt");
        if (place_it.isCategory()) {
        	setContentView(R.layout.view_place_it_cat);
        	placeIt = (PlaceIt) dataSource.findByPinId(place_it.getPlaceItId());
        	setUpCategoryUI();
        	
        }
        else {
        	setContentView(R.layout.view_place_it);
        	placeIt = dataSource.findByLatLong(place_it.getLatitude(), place_it.getLongitude());
        	setUpRegularUI();
        	
        }
        setUpUIElements();
	}

	private View.OnClickListener onClickListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			switch(v.getId())
			{
				case R.id.setActiveBtn:
					placeIt.setStatus("ACTIVE");
					dataSource.editById(placeIt);
					finish();
				break;
			
				case R.id.setInactiveBtn:
					placeIt.setStatus("INACTIVE");
					Calendar gc  = new GregorianCalendar();
					gc.set(Calendar.HOUR_OF_DAY, 0);
					gc.set(Calendar.SECOND, 0);
					gc.set(Calendar.MINUTE, 0);
					gc.set(Calendar.DATE,0);
					placeIt.setInactiveDateTime(gc);
					dataSource.editById(placeIt);
					finish();	
				break;
				
				case R.id.deleteBtn:
					dataSource.deleteById(placeIt.getPlaceItId());
					finish();
				break;
			}
			if (!placeIt.isCategory())
				MainActivity.setReturnView((PlaceIt)placeIt);
		}
	};

	private void setUpUIElements()
	{
		TextView title_content;
		TextView description_content;
		
		setActiveButton = (Button)findViewById(R.id.setActiveBtn);
	    setActiveButton.setOnClickListener(onClickListener);
	    setInactive = (Button)findViewById(R.id.setInactiveBtn);
	    setInactive.setOnClickListener(onClickListener);
	    deleteButton = (Button)findViewById(R.id.deleteBtn);
	    deleteButton.setOnClickListener(onClickListener);
	    title_content = (TextView)findViewById(R.id.titleContent);
	    description_content = (TextView)findViewById(R.id.descriptionContent);
	    title_content.setText(placeIt.getTitle());
	    description_content.setText(placeIt.getDescription());
	
	}
	private void setUpRegularUI() {
		TextView show_schedule;
		String schedule;
		CheckBox check_box;
		
	    show_schedule = (TextView)findViewById(R.id.showSchedule);
	    check_box = (CheckBox)findViewById(R.id.checkBox1);
	    
	   
	    schedule = "";
	    if (placeIt.getSchedule() != 0) 
	    	schedule += placeIt.getSchedule();
	    show_schedule.setText(schedule);
	    if (placeIt.getOnSchedule() == 1) 
	    	check_box.setChecked(true);
	    else
	    	check_box.setChecked(false);
	}
	private void setUpCategoryUI() {
		TextView cat1;
		TextView cat2;
		TextView cat3;
		cat1 = (TextView)findViewById(R.id.category1);
		cat1.setText("Category 1: " + placeIt.getCategory1());
		cat2 = (TextView)findViewById(R.id.category2);
		cat2.setText("Category 2: " + placeIt.getCategory2());
		cat3 = (TextView)findViewById(R.id.category3);
		cat3.setText("Category 3: " + placeIt.getCategory3());
	}
}
